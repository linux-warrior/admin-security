# -*- coding: utf-8 -*-
from collections import OrderedDict
import unittest

from .gamma import GammaCipher


class GammaCipherTestCase(unittest.TestCase):
    def test_russian_dict(self):
        self.assertEqual(len(GammaCipher.RUSSIAN_DICT), 32)
        self.assertEqual(len(set(GammaCipher.RUSSIAN_DICT.values())), 32)

    def test_russian_line(self):
        data = 'будь'
        gamma = [7, 1, 8, 2]
        alphabet_dict = OrderedDict([
            ('б', 0b010010), ('у', 0b100000), ('д', 0b110010), ('ь', 0b100100),
        ])
        cipher = GammaCipher(gamma, alphabet_dict=alphabet_dict)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = '010101 100001 111010 100110'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        self.assertEqual(decrypted_data, data)

    def test_exercise(self):
        data_list = [
            'Помехоустойчивое кодирование – ',
            'это кодирование с возможностью ',
            'восстановления потерянных ',
            'или ошибочно принятых данных.',
        ]
        gamma = [2, 3, 10, 4, 1, 5, 6, 7, 8, 11, 15, 14, 12, 13, 9, 0]
        cipher = GammaCipher(gamma)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = [
            '010001 000111 011011 000110 011001 000001 000011 010010 '
            '011110 001111 001111 010100 001111 000111 001101 000010 '
            '001101 000111 000110 000111 010101 000001 001100 000110 '
            '011010 001000 001101',

            '001000 011010 001001 000110 000100 001110 000000 011110 '
            '000000 001011 000100 010100 000100 001010 011110 000101 '
            '001010 000010 011100 001101 001101 010000 000111 011111 '
            '010010 011111 000010',

            '001100 000011 011101 011110 011001 001111 011110 001001 '
            '000011 010000 000000 010001 001001 001100 010010 000001 '
            '010000 000101 011100 000011 011101 011100 010001 010101',

            '001010 010000 000001 000111 010001 000111 001000 000001 '
            '011100 010101 001100 011000 011011 001101 011110 000101 '
            '011111 011101 011010 001111 001011 010110 010011 011000 '
            '011110',
        ]
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        expected_decrypted_data_list = [
            'помехоустойчивоекодирование',
            'этокодированиесвозможностью',
            'восстановленияпотерянных',
            'илиошибочнопринятыхданных',
        ]
        self.assertListEqual(decrypted_data_list, expected_decrypted_data_list)
