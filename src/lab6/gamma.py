#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import OrderedDict
import itertools
import re
import sys


class GammaCipher:
    RUSSIAN_DICT = OrderedDict([
        ('а', 0b000001), ('б', 0b001001), ('в', 0b001010), ('г', 0b001011),
        ('д', 0b001100), ('е', 0b000010), ('ж', 0b001101), ('з', 0b001110),
        ('и', 0b000011), ('й', 0b000000), ('к', 0b001111), ('л', 0b010000),
        ('м', 0b010001), ('н', 0b010010), ('о', 0b000100), ('п', 0b010011),
        ('р', 0b010100), ('с', 0b010101), ('т', 0b010110), ('у', 0b000101),
        ('ф', 0b010111), ('х', 0b011000), ('ц', 0b011001), ('ч', 0b011010),
        ('ш', 0b011011), ('щ', 0b011100), ('ъ', 0b111111), ('ы', 0b011101),
        ('ь', 0b011110), ('э', 0b000110), ('ю', 0b000111), ('я', 0b001000),
    ])

    def __init__(self, gamma, alphabet_dict=None):
        if alphabet_dict is None:
            alphabet_dict = self.RUSSIAN_DICT
        self.alphabet_dict = alphabet_dict
        self.alphabet_dict_reverse = OrderedDict((v, k) for (k, v) in self.alphabet_dict.items())
        self.gamma = gamma

        self.reset()

    def reset(self):
        self._gamma_char_iter = itertools.cycle(self.gamma)

    def encrypt(self, source, reset=True):
        if reset:
            self.reset()

        result = []

        for source_char in source:
            try:
                source_char_code = self.alphabet_dict[source_char.lower()]
            except KeyError:
                continue

            gamma_char_code = next(self._gamma_char_iter)
            result_char_code = source_char_code ^ gamma_char_code
            result.append('{0:06b}'.format(result_char_code))

        return ' '.join(result)

    def _split_numbers(self, source):
        for match_obj in re.finditer(r'\S+', source):
            token = match_obj.group(0)
            if not re.match(r'^[01]+$', token):
                raise ValueError('Элемент строки не является двоичным числом: "{}".'.format(token))

            yield int(token, 2)

    def decrypt(self, source, reset=True):
        if reset:
            self.reset()

        result = []

        for source_char_code in self._split_numbers(source):
            gamma_char_code = next(self._gamma_char_iter)
            result_char_code = source_char_code ^ gamma_char_code
            try:
                result_char = self.alphabet_dict_reverse[result_char_code]
            except KeyError:
                raise ValueError('Неправильный код символа: "{}".'.format(result_char_code))
            result.append(result_char)

        return ''.join(result)

    def _process(self, source, reset=True, decrypt=False):
        if not decrypt:
            return self.encrypt(source, reset=reset)
        else:
            return self.decrypt(source, reset=reset)

    def encrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=False)

    def decrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=True)

    def _process_lines(self, source_lines, decrypt=False):
        self.reset()
        for line in source_lines:
            line = line.strip()
            yield self._process(line, reset=False, decrypt=decrypt)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Шифрует методом гаммирования текст, переданный в стандартный ввод.'
    )
    parser.add_argument(
        '--decrypt',
        action='store_true',
        default=False,
        help='Включить режим дешифрования.'
    )
    parser.add_argument(
        'gamma',
        metavar='int',
        type=int,
        nargs='+',
        help='Гамма-последовательность.'
    )
    args = parser.parse_args()

    cipher = GammaCipher(args.gamma)
    if not args.decrypt:
        results_iter = cipher.encrypt_lines(sys.stdin)
    else:
        results_iter = cipher.decrypt_lines(sys.stdin)

    for result in results_iter:
        print(result)
