#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import OrderedDict
import itertools
import operator
import re
import string
import sys


class Alphabet:
    def __init__(self, cipher, english=False, alphabet=None):
        self.cipher = cipher

        if english:
            alphabet = string.ascii_lowercase
        elif alphabet is None:
            # По умолчанию используем русский алфавит
            alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        self.alphabet = alphabet.lower()

        self.dict = OrderedDict((char, i) for (i, char) in enumerate(self.alphabet))

    def __contains__(self, char):
        return char in self.dict

    def __getitem__(self, char):
        return self.dict[char]


class KeywordChar:
    def __init__(self, keyword, char, position):
        self.keyword = keyword
        self.alphabet = self.keyword.alphabet
        self.char = char
        self.position = position

    @property
    def key(self):
        return self.alphabet[self.char], self.position


class Keyword:
    def __init__(self, cipher, keyword):
        self.cipher = cipher
        self.alphabet = self.cipher.alphabet
        self.keyword = keyword.lower()
        if not all(char in self.alphabet for char in self.keyword):
            raise ValueError('Ключевое слово должно состоять из символов алфавита.')

        self.char_list = [KeywordChar(self, char, i) for (i, char) in enumerate(self.keyword)]
        self.char_list.sort(key=operator.attrgetter('key'))

    def __len__(self):
        return len(self.keyword)

    @property
    def positions_list(self):
        return map(operator.attrgetter('position'), self.char_list)


class TranspositionCipher:
    def __init__(self, keyword, english=False, alphabet=None):
        self.alphabet = Alphabet(self, english=english, alphabet=alphabet)
        self.keyword = Keyword(self, keyword)

    def encrypt(self, source):
        char_table = []
        for i in range(len(self.keyword)):
            char_table.append([])

        source_iter = map(operator.methodcaller('lower'), iter(source))
        source_iter = filter(lambda char: char in self.alphabet, source_iter)
        while True:
            row = list(itertools.islice(source_iter, len(self.keyword)))
            if not row:
                break

            for position, char in enumerate(row):
                char_table[position].append(char)

        result = []
        for position in self.keyword.positions_list:
            column = char_table[position]
            result.append(''.join(column))

        return ' '.join(result)

    def _split_columns(self, source):
        for match_obj in re.finditer(r'\S+', source):
            column = match_obj.group(0)
            yield column

    def decrypt(self, source):
        char_table = []
        for i in range(len(self.keyword)):
            char_table.append([])

        source_iter = self._split_columns(source)
        max_column_len = None
        for position in self.keyword.positions_list:
            column = next(source_iter, None)
            if column is None:
                break

            char_table[position] = column
            if max_column_len is None or len(column) > max_column_len:
                max_column_len = len(column)

        if not max_column_len:
            return ''

        result = []
        for row_num in range(max_column_len):
            for position in range(len(self.keyword)):
                column = char_table[position]
                if row_num < len(column):
                    result.append(column[row_num])

        return ''.join(result)

    def _process(self, source, decrypt=False):
        if not decrypt:
            return self.encrypt(source)
        else:
            return self.decrypt(source)

    def encrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=False)

    def decrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=True)

    def _process_lines(self, source_lines, decrypt=False):
        for line in source_lines:
            line = line.strip()
            yield self._process(line, decrypt=decrypt)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Шифрует методом перестановки текст, переданный в стандартный ввод.'
    )
    parser.add_argument(
        '--decrypt',
        action='store_true',
        default=False,
        help='Включить режим дешифрования.'
    )
    parser.add_argument(
        '--english',
        action='store_true',
        default=False,
        help='Использовать английский алфавит.'
    )
    parser.add_argument(
        '--alphabet',
        help='Алфавит (по умолчанию будет применен русский).'
    )
    parser.add_argument('keyword', help="Ключевое слово.")
    args = parser.parse_args()

    cipher = TranspositionCipher(args.keyword, english=args.english, alphabet=args.alphabet)
    if not args.decrypt:
        results_iter = cipher.encrypt_lines(sys.stdin)
    else:
        results_iter = cipher.decrypt_lines(sys.stdin)

    for result in results_iter:
        print(result)
