import unittest

from .transposition import TranspositionCipher


class TranspositionCipherTestCase(unittest.TestCase):
    def test_russian_line(self):
        data = 'пусть будет так, как мы хотели'
        keyword = 'радиатор'
        cipher = TranspositionCipher(keyword)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = 'уты ькт стх тао уал пем дки бке'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        expected_decrypted_data = 'пустьбудеттаккакмыхотели'
        self.assertEqual(decrypted_data, expected_decrypted_data)

    def test_english_line(self):
        data = 'Common sense is not so common.'
        keyword = 'abcdefgh'
        cipher = TranspositionCipher(keyword, english=True)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = 'cns oso mec mio osm nnm soo etn'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        expected_decrypted_data = 'commonsenseisnotsocommon'
        self.assertEqual(decrypted_data, expected_decrypted_data)

    def test_russian_lines_list(self):
        data_list = [
            'пусть будет так, ',
            'как мы хотели',
        ]
        keyword = 'радиатор'
        cipher = TranspositionCipher(keyword)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = [
            'ут ьк ст та у пе д б',
            'ал ы ки м о ке т х',
        ]
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        expected_decrypted_data_list = [
            'пустьбудеттак',
            'какмыхотели',
        ]
        self.assertListEqual(decrypted_data_list, expected_decrypted_data_list)

    def test_english_lines_list(self):
        data_list = [
            'Common sense is ',
            'not so common.',
        ]
        keyword = 'abcdefgh'
        cipher = TranspositionCipher(keyword, english=True)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = [
            'cn os me mi os n s e',
            'nm oo tn s o c o m',
        ]
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        expected_decrypted_data_list = [
            'commonsenseis',
            'notsocommon',
        ]
        self.assertListEqual(decrypted_data_list, expected_decrypted_data_list)

    def test_exercise(self):
        data = (
            'Двадцать первое. Ночь. Понедельник. Очертанья столицы во мгле. '
            'Сочинил же какой-то бездельник, что бывает любовь на земле. '
            'И от лености или от скуки все поверили, так и живут: '
            'ждут свиданий, боятся разлуки и любовные песни поют.'
        )
        keyword = 'головоломка'
        cipher = TranspositionCipher(keyword)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = (
            'рерциткюеисатяис цонягедынесеваавю двдтыночбилекстлн '
            'енеичйилливтуоие аелнолеовтоожиябп тьктеалазоуитилы '
            'почлоонтмтиидбкп воеавибтооипивсюи днььмжзбьлтвидроо '
            'ачислкеванкрунзнт ьпооскьеесклжйуе'
        )
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        expected_decrypted_data = (
            'двадцатьпервоеночьпонедельникочертаньястолицывомгле'
            'сочинилжекакойтобездельникчтобываетлюбовьназемле'
            'иотленостиилиотскукивсеповерилитакиживут'
            'ждутсвиданийбоятсяразлукиилюбовныепеснипоют'
        )
        self.assertEqual(decrypted_data, expected_decrypted_data)
