# -*- coding: utf-8 -*-
import unittest

from .matrix import MatrixCipher


class MatrixCipherTestCase(unittest.TestCase):
    def test_russian_line(self):
        data = 'ватала'
        matrix = [[14, 8, 3], [8, 5, 2], [3, 2, 1]]
        cipher = MatrixCipher(matrix)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = '85 54 25 96 60 24'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        self.assertEqual(decrypted_data, data)

    def test_russian_line_padding(self):
        data = 'ваталав'
        matrix = [[14, 8, 3], [8, 5, 2], [3, 2, 1]]
        cipher = MatrixCipher(matrix)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = '85 54 25 96 60 24 391 247 105'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        self.assertEqual(decrypted_data, data)

    def test_russian_lines_list(self):
        data_list = ['ва', 'та', 'ла']
        matrix = [[14, 8, 3], [8, 5, 2], [3, 2, 1]]
        cipher = MatrixCipher(matrix)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = ['85 54 25', '96 60 24']
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        expected_decrypted_data_list = ['ват', 'ала']
        self.assertListEqual(decrypted_data_list, expected_decrypted_data_list)

    def test_russian_lines_list_padding(self):
        data_list = ['ва', 'та', 'лав']
        matrix = [[14, 8, 3], [8, 5, 2], [3, 2, 1]]
        cipher = MatrixCipher(matrix)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = ['85 54 25', '96 60 24 391 247 105']
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        expected_decrypted_data_list = ['ват', 'алав']
        self.assertListEqual(decrypted_data_list, expected_decrypted_data_list)

    def test_exercise(self):
        data = (
            'Помехоустойчивое кодирование – это кодирование с возможностью '
            'восстановления потерянных или ошибочно принятых данных.'
        )
        matrix = [11, 7, 14, 13, 4, 13, 6, 15, 4]
        cipher = MatrixCipher(matrix)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = (
            '463 437 373 419 348 420 612 579 466 571 547 336 323 320 144 342 '
            '304 255 345 309 227 179 203 120 287 283 239 673 661 525 282 255 '
            '307 428 380 369 218 208 68 386 371 201 239 190 269 346 320 331 '
            '511 476 381 846 766 673 379 320 309 331 310 393 287 268 317 363 '
            '358 203 547 453 598 368 336 395 607 531 638 658 580 592 309 282 '
            '270 466 412 501 452 385 327 483 450 373 446 439 293 877 856 589 '
            '270 302 192 644 602 406 935 847 759'
        )
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        expected_decrypted_data = (
            'помехоустойчивоекодированиеэтокодированиесвозможностью'
            'восстановленияпотерянныхилиошибочнопринятыхданных'
        )
        self.assertEqual(decrypted_data, expected_decrypted_data)
