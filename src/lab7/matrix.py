#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import OrderedDict
import itertools
import math
import operator
import re
import string
import sys

import numpy as np


class HasNextIter:
    def __init__(self, it):
        self.it = iter(it)
        self._has_next = self._next = None

    def __iter__(self):
        return self

    def __next__(self):
        result = self._next if self._has_next else next(self.it)
        self._has_next = None
        return result

    @property
    def has_next(self):
        if self._has_next is None:
            try:
                self._next = next(self.it)
            except StopIteration:
                self._has_next = False
            else:
                self._has_next = True

        return self._has_next


class MatrixCipher:
    def __init__(self, matrix, english=False, alphabet=None):
        if english:
            alphabet = string.ascii_lowercase
        elif alphabet is None:
            # По умолчанию используем русский алфавит
            alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        self.alphabet = alphabet.lower()
        self.alphabet_dict = OrderedDict((char, i) for (i, char) in enumerate(self.alphabet))
        self.padding = len(self.alphabet)

        self.matrix = np.array(list(matrix), dtype=int)
        matrix_order = math.sqrt(self.matrix.size)
        if matrix_order != int(matrix_order):
            raise ValueError('Ключевая матрица должна быть квадратной.')
        self.matrix_order = int(matrix_order)
        self.matrix.resize((self.matrix_order, self.matrix_order))
        try:
            self.matrix_inv = np.linalg.inv(self.matrix)
        except np.linalg.LinAlgError:
            raise ValueError('Ключевая матрица вырожденная.')

        self.reset()

    def reset(self):
        self._encrypt_source_buffer = self._decrypt_source_buffer = None

    def encrypt(self, source, more=False):
        result = []

        source_iter = map(operator.methodcaller('lower'), iter(source))
        source_iter = filter(lambda char: char in self.alphabet, source_iter)
        if self._encrypt_source_buffer:
            source_iter = itertools.chain(self._encrypt_source_buffer, source_iter)
            self._encrypt_source_buffer = None

        while True:
            source_chunk = list(itertools.islice(source_iter, self.matrix_order))
            if not source_chunk:
                break
            if len(source_chunk) < self.matrix_order and more:
                self._encrypt_source_buffer = source_chunk
                break

            source_vector = np.array(list(map(lambda char: self.alphabet_dict[char], source_chunk)))
            if source_vector.size < self.matrix_order:
                source_vector = np.pad(
                    source_vector,
                    (0, self.matrix_order - source_vector.size),
                    'constant',
                    constant_values=(0, self.padding)
                )

            result_vector = self.matrix.dot(source_vector)
            result_chunk = ' '.join(map(str, result_vector))
            result.append(result_chunk)

        return ' '.join(result)

    def _split_numbers(self, source):
        for match_obj in re.finditer(r'\S+', source):
            token = match_obj.group(0)
            if not token.isdigit():
                raise ValueError('Элемент строки не является числом: "{}".'.format(token))

            yield int(token)

    def decrypt(self, source, more=False):
        result = []

        source_iter = self._split_numbers(source)
        if self._decrypt_source_buffer:
            source_iter = itertools.chain(self._decrypt_source_buffer, source_iter)
            self._decrypt_source_buffer = None

        while True:
            source_chunk = list(itertools.islice(source_iter, self.matrix_order))
            if not source_chunk:
                break
            if len(source_chunk) < self.matrix_order:
                if more:
                    self._decrypt_source_buffer = source_chunk
                    break
                else:
                    raise ValueError('Недостаточно чисел в строке.')

            source_vector = np.array(source_chunk)
            result_vector = self.matrix_inv.dot(source_vector)
            result_vector = np.rint(result_vector).astype(int)
            for char_code in result_vector:
                if char_code == self.padding:
                    continue
                try:
                    result_char = self.alphabet[char_code]
                except IndexError:
                    raise ValueError('Неправильный код символа: "{}".'.format(char_code))
                result.append(result_char)

        return ''.join(result)

    def _process(self, source, more=False, decrypt=False):
        if not decrypt:
            return self.encrypt(source, more=more)
        else:
            return self.decrypt(source, more=more)

    def encrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=False)

    def decrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=True)

    def _process_lines(self, source_lines, decrypt=False):
        self.reset()
        source_lines_iter = HasNextIter(source_lines)
        for line in source_lines_iter:
            line = line.strip()
            result = self._process(line, more=source_lines_iter.has_next, decrypt=decrypt)
            if result:
                yield result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=(
            'Шифрует или дешифрует с помощью аналитических преобразований '
            'текст, переданный в стандартный ввод.'
        )
    )
    parser.add_argument(
        '--decrypt',
        action='store_true',
        default=False,
        help='Включить режим дешифрования.'
    )
    parser.add_argument(
        '--english',
        action='store_true',
        default=False,
        help='Использовать английский алфавит.'
    )
    parser.add_argument(
        '--alphabet',
        help='Алфавит (по умолчанию будет применен русский).'
    )
    parser.add_argument(
        'matrix',
        metavar='int',
        type=int,
        nargs='+',
        help='Ключевая матрица.'
    )
    args = parser.parse_args()

    cipher = MatrixCipher(args.matrix, english=args.english, alphabet=args.alphabet)
    if not args.decrypt:
        results_iter = cipher.encrypt_lines(sys.stdin)
    else:
        results_iter = cipher.decrypt_lines(sys.stdin)

    for result in results_iter:
        print(result)
