# -*- coding: utf-8 -*-
import unittest

from .vigenere import VigenereCipher


class VigenereCipherTestCase(unittest.TestCase):
    def test_russian_line(self):
        data = 'от улыбки каждый день светлей'
        keyword = 'мышь'
        cipher = VigenereCipher(keyword)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = 'ын лззьге чыяазе ьбъч йюсндбц'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        self.assertEqual(decrypted_data, data)

    def test_english_line(self):
        data = 'Common sense is not so common.'
        keyword = 'pizza'
        cipher = VigenereCipher(keyword, english=True)

        encrypted_data = cipher.encrypt(data)
        expected_encrypted_data = 'Rwlloc admst qr moi an bobunm.'
        self.assertEqual(encrypted_data, expected_encrypted_data)

        decrypted_data = cipher.decrypt(encrypted_data)
        self.assertEqual(decrypted_data, data)

    def test_russian_lines_list(self):
        data_list = ['от улыбки', 'каждый день', 'светлей']
        keyword = 'мышь'
        cipher = VigenereCipher(keyword)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = ['ын лззьге', 'чыяазе ьбъч', 'йюсндбц']
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        self.assertListEqual(decrypted_data_list, data_list)

    def test_english_lines_list(self):
        data_list = ['Common sense', 'is not', 'so common.']
        keyword = 'pizza'
        cipher = VigenereCipher(keyword, english=True)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = ['Rwlloc admst', 'qr moi', 'an bobunm.']
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        self.assertListEqual(decrypted_data_list, data_list)

    def test_exercise1(self):
        data_list = [
            'Ей рано нравились романы',
            'Они ей заменяли все',
            'Она влюблялася в обманы',
            'И Ричардсона и Руссо',
        ]
        keyword = 'идеал'
        cipher = VigenereCipher(keyword)

        encrypted_data_list = list(cipher.encrypt_lines(data_list))
        expected_encrypted_data_list = [
            'Нн хащч сханспнсз щтсащд',
            'Тти рт лемрцгри нъи',
            'Унл кпгбчзпеск к тёмлця',
            'Н Рфадхдэчсе и Ььхцо',
        ]
        self.assertListEqual(encrypted_data_list, expected_encrypted_data_list)

    def test_exercise2(self):
        encrypted_data_list = [
            'Фу эъцё сйу дюсягм ьръгм',
            'С аяцшффжмм схщм зпаэпдпьйс',
            'Ню т щхитрд хе сщтзл сбула',
        ]
        keyword = 'проза'
        cipher = VigenereCipher(keyword)

        decrypted_data_list = list(cipher.decrypt_lines(encrypted_data_list))
        # Вероятно, в методичке была опечатка, поэтому результат получается не
        # очень красивый.
        expected_decrypted_data_list = [
            'Ег отцц был добрым малым',
            'В прошедшем веке запоздалый',
            'Но в книгах не видал вреда',
        ]
        self.assertListEqual(decrypted_data_list, expected_decrypted_data_list)
