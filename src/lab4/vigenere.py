#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import OrderedDict
import itertools
import string
import sys


class VigenereCipher:
    def __init__(self, keyword, english=False, alphabet=None):
        if english:
            alphabet = string.ascii_lowercase
        elif alphabet is None:
            # По умолчанию используем русский алфавит
            alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        self.alphabet = alphabet.lower()
        self.alphabet_dict = OrderedDict((char, i) for (i, char) in enumerate(self.alphabet))

        self.keyword = keyword.lower()
        if not all(keyword_char in self.alphabet_dict for keyword_char in self.keyword):
            raise ValueError('Ключевое слово должно состоять из символов алфавита.')

        self.reset()

    def reset(self):
        self._keyword_char_iter = itertools.cycle(self.keyword)

    def encrypt(self, source, reset=True):
        return self._process(source, reset=reset, decrypt=False)

    def decrypt(self, source, reset=True):
        return self._process(source, reset=reset, decrypt=True)

    def _process(self, source, reset=True, decrypt=False):
        if reset:
            self.reset()

        result = []
        for source_char in source:
            try:
                result_char_code = self.alphabet_dict[source_char.lower()]
            except KeyError:
                result.append(source_char)
                continue

            keyword_char = next(self._keyword_char_iter)
            keyword_char_code = self.alphabet_dict[keyword_char]

            if not decrypt:
                result_char_code += keyword_char_code
            else:
                result_char_code -= keyword_char_code

            result_char_code %= len(self.alphabet)
            result_char = self.alphabet[result_char_code]

            if source_char.isupper():
                result_char = result_char.upper()

            result.append(result_char)

        return ''.join(result)

    def encrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=False)

    def decrypt_lines(self, source_lines):
        return self._process_lines(source_lines, decrypt=True)

    def _process_lines(self, source_lines, decrypt=False):
        self.reset()
        for line in source_lines:
            line = line.strip()
            yield self._process(line, reset=False, decrypt=decrypt)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=(
            'Шифрует или дешифрует с помощью алгоритма Виженера текст, '
            'переданный в стандартный ввод.'
        )
    )
    parser.add_argument(
        '--decrypt',
        action='store_true',
        default=False,
        help='Включить режим дешифрования.'
    )
    parser.add_argument(
        '--english',
        action='store_true',
        default=False,
        help='Использовать английский алфавит.'
    )
    parser.add_argument(
        '--alphabet',
        help='Алфавит (по умолчанию будет применен русский).'
    )
    parser.add_argument('keyword', help="Ключевое слово.")
    args = parser.parse_args()

    cipher = VigenereCipher(args.keyword, english=args.english, alphabet=args.alphabet)
    if not args.decrypt:
        results_iter = cipher.encrypt_lines(sys.stdin)
    else:
        results_iter = cipher.decrypt_lines(sys.stdin)

    for result in results_iter:
        print(result)
